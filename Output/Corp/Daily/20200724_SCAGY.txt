EDI_SCAGY_20200724
TableName	Actflag	Created	Changed	ScagyID	SecID	ISIN	Relationship	AgncyID	GuaranteeType	SpStartDate	SpEndDate	Notes
SCAGY	I	2020/07/24	2020/07/24	3138736	6230785	XS2095251430	TST	4757				
SCAGY	I	2020/07/24	2020/07/24	3138737	6230785	XS2095251430	PA	4757				
SCAGY	I	2020/07/24	2020/07/24	3138738	6230785	XS2095251430	REG	36989				
SCAGY	I	2020/07/24	2020/07/24	3138739	6230785	XS2095251430	BC	44610				LEGAL ADVISERS TO THE ISSUER AND THE COMPANY<BR>As to English law
SCAGY	I	2020/07/24	2020/07/24	3138740	6230785	XS2095251430	GUR	45146	GTD			
SCAGY	I	2020/07/24	2020/07/24	3138742	6230785	XS2095251430	MGR	45148				
SCAGY	I	2020/07/24	2020/07/24	3138743	6230785	XS2095251430	UNW	45148				
SCAGY	I	2020/07/24	2020/07/24	3138744	6230785	XS2095251430	GUR	45148	BPA			
SCAGY	I	2020/07/24	2020/07/24	3138745	6230785	XS2095251430	GUR	45147	SBLC			
SCAGY	I	2020/07/24	2020/07/24	3138746	6230785	XS2095251430	DEP	5334				
SCAGY	I	2020/07/24	2020/07/24	3138747	6230785	XS2095251430	DEP	41512				
SCAGY	I	2020/07/24	2020/07/24	3138777	6284582	XS2109438205	REG	36989				
SCAGY	I	2020/07/24	2020/07/24	3138778	6284582	XS2109438205	PA	40400				
SCAGY	I	2020/07/24	2020/07/24	3138779	6284582	XS2109438205	TST	44538				
SCAGY	I	2020/07/24	2020/07/24	3138780	6284582	XS2109438205	BC	23045				LEGAL ADVISER<BR>To the Company as to U.S. Federal and English law
SCAGY	I	2020/07/24	2020/07/24	3138796	6284582	XS2109438205	GUR	45151	OBG			
SCAGY	I	2020/07/24	2020/07/24	3138797	6284582	XS2109438205	MGR	17160				
SCAGY	I	2020/07/24	2020/07/24	3138798	6284582	XS2109438205	UNW	17160				
SCAGY	I	2020/07/24	2020/07/24	3138946	6284582	XS2109438205	GUR	17160	BPA			
SCAGY	I	2020/07/24	2020/07/24	3139798	6284379	XS2115155033	TST	35131				
SCAGY	I	2020/07/24	2020/07/24	3139799	6284379	XS2115155033	REG	1392				
SCAGY	I	2020/07/24	2020/07/24	3139800	6284379	XS2115155033	BC	41382				
SCAGY	I	2020/07/24	2020/07/24	3139809	6284379	XS2115155033	DEP	5334				
SCAGY	I	2020/07/24	2020/07/24	3139810	6284379	XS2115155033	DEP	41512				
SCAGY	I	2020/07/24	2020/07/24	3139811	6284379	XS2115155033	DEP	35695				
SCAGY	I	2020/07/24	2020/07/24	3139825	6284379	XS2115155033	GUR	41536	GTD			
SCAGY	I	2020/07/24	2020/07/24	3139826	6284379	XS2115155033	GUR	39219	GTD			
SCAGY	I	2020/07/24	2020/07/24	3139827	6284379	XS2115155033	GUR	39218	GTD			
SCAGY	I	2020/07/24	2020/07/24	3139831	6284379	XS2115155033	GUR	39132	BPA			
SCAGY	I	2020/07/24	2020/07/24	3139832	6284379	XS2115155033	UNW	39132				
SCAGY	I	2020/07/24	2020/07/24	3138782	6444645	US91533BAD29	DEP	5158				
SCAGY	I	2020/07/24	2020/07/24	3138964	6474753	US74348Y5K30	TST	42618				
SCAGY	I	2020/07/24	2020/07/24	3138965	6474753	US74348Y5K30	GUR	3501	BPA			
SCAGY	I	2020/07/24	2020/07/24	3138966	6474753	US74348Y5K30	UNW	3501				
SCAGY	I	2020/07/24	2020/07/24	3138967	6474753	US74348Y5K30	CUST	44520				
SCAGY	I	2020/07/24	2020/07/24	3138968	6474753	US74348Y5K30	REG	7424				
SCAGY	I	2020/07/24	2020/07/24	3138969	6474753	US74348Y5K30	PA	7424				
SCAGY	I	2020/07/24	2020/07/24	3139089	6474750	US74348Y5J66	GUR	3501	BPA			
SCAGY	I	2020/07/24	2020/07/24	3139090	6474750	US74348Y5J66	UNW	3501				
SCAGY	I	2020/07/24	2020/07/24	3139091	6474750	US74348Y5J66	CUST	44520				
SCAGY	I	2020/07/24	2020/07/24	3139092	6474750	US74348Y5J66	DEP	5158				
SCAGY	I	2020/07/24	2020/07/24	3139093	6474750	US74348Y5J66	PA	7424				
SCAGY	I	2020/07/24	2020/07/24	3139094	6474750	US74348Y5J66	REG	7424				
SCAGY	I	2020/07/24	2020/07/24	3139095	6474750	US74348Y5J66	TST	42618				
SCAGY	I	2020/07/24	2020/07/24	3139070	6474744	US74348Y5H01	CUST	44520				
SCAGY	I	2020/07/24	2020/07/24	3139071	6474744	US74348Y5H01	DEP	5158				
SCAGY	I	2020/07/24	2020/07/24	3139072	6474744	US74348Y5H01	PA	7424				
SCAGY	I	2020/07/24	2020/07/24	3139073	6474744	US74348Y5H01	REG	7424				
SCAGY	I	2020/07/24	2020/07/24	3139074	6474744	US74348Y5H01	TST	42618				
SCAGY	I	2020/07/24	2020/07/24	3139075	6474744	US74348Y5H01	UNW	3501				
SCAGY	I	2020/07/24	2020/07/24	3139076	6474744	US74348Y5H01	GUR	3501	BPA			
SCAGY	I	2020/07/24	2020/07/24	3138758	6487966	US526107AF41	MGR	5334				
SCAGY	I	2020/07/24	2020/07/24	3138754	6487964	US526107AE75	MGR	5334				
SCAGY	I	2020/07/17	2020/07/17	3129540	6479948	US69121KAD63	MGR	33053				
SCAGY	I	2020/07/20	2020/07/20	3133012	6479948	US69121KAD63	DEP	5158				
SCAGY	I	2020/07/20	2020/07/20	3133013	6479948	US69121KAD63	TST	6938				
SCAGY	I	2020/07/20	2020/07/20	3133014	6479948	US69121KAD63	PA	6938				
SCAGY	I	2020/07/20	2020/07/20	3133015	6479948	US69121KAD63	REG	6938				
SCAGY	I	2020/07/20	2020/07/20	3133016	6479948	US69121KAD63	UNW	42377				
SCAGY	I	2020/07/20	2020/07/20	3133017	6479948	US69121KAD63	GUR	42377	BPA			
SCAGY	I	2020/07/20	2020/07/20	3133018	6479948	US69121KAD63	BC	40734				
SCAGY	I	2020/07/21	2020/07/21	3134396	6484554	US89236THD00	MGR	37216				
SCAGY	I	2020/07/22	2020/07/22	3135807	6484554	US89236THD00	DEP	5158				
SCAGY	I	2020/07/22	2020/07/22	3135808	6484554	US89236THD00	UNW	37216				
SCAGY	I	2020/07/22	2020/07/22	3135809	6484554	US89236THD00	GUR	37216	BPA			
SCAGY	I	2020/07/22	2020/07/22	3135810	6484554	US89236THD00	BC	45124				
SCAGY	I	2020/07/24	2020/07/24	3138939	6489515	US05967CAF05	MGR	3863				
SCAGY	I	2020/07/24	2020/07/24	3139061	6490194	US13161GY556	DEP	5158				
SCAGY	I	2020/07/24	2020/07/24	3138781	6489432	US22411VAP31	MGR	13545				
SCAGY	I	2020/07/24	2020/07/24	3138775	6489425	US22411WAP14	MGR	13545				
SCAGY	I	2020/07/24	2020/07/24	3139023	6490147	US064159VD54	BC	9259				
SCAGY	I	2020/07/24	2020/07/24	3139024	6490147	US064159VD54	DEP	5158				
SCAGY	I	2020/07/24	2020/07/24	3139025	6490147	US064159VD54	CAL	3795				
SCAGY	I	2020/07/24	2020/07/24	3139026	6490147	US064159VD54	GUR	7595	BPA			
SCAGY	I	2020/07/24	2020/07/24	3139027	6490147	US064159VD54	UNW	7595				
SCAGY	I	2020/07/24	2020/07/24	3139839	6490726	US06367WX656	CAL	7878				
SCAGY	I	2020/07/24	2020/07/24	3139840	6490726	US06367WX656	UNW	7878				
SCAGY	I	2020/07/24	2020/07/24	3139858	6490726	US06367WX656	BC	9259				In the opinion of Osler, Hoskin & Harcourt LLP, the issue and sale of the notes has been duly authorized by all necessary corporate action of the Bank in conformity<BR>with the Senior Indenture, and when this pricing supplement has been attached to, and duly notated on, the master note that represents the notes, the notes will have been validly<BR>executed and issued and, to the extent validity of the notes is a matter governed by the laws of the Province of Ontario, or the laws of Canada applicable therein, and will be valid<BR>obligations of the Bank, subject to the following limitations (i) the enforceability of the Senior Indenture may be limited by the Canada Deposit Insurance Corporation Act<BR>(Canada), the Winding-up and Restructuring Act (Canada) and bankruptcy, insolvency, reorganization, receivership, moratorium, arrangement or winding-up laws or other<BR>similar laws affecting the enforcement of creditors  rights generally; (ii) the enforceability of the Senior Indenture may be limited by equitable principles, including the principle<BR>that equitable remedies such as specific performance and injunction may only be granted in the discretion of a court of competent jurisdiction; (iii) pursuant to the Currency Act<BR>(Canada) a judgment by a Canadian court must be awarded in Canadian currency and that such judgment may be based on a rate of exchange in existence on a day other than the<BR>day of payment; and (iv) the enforceability of the Senior Indenture will be subject to the limitations contained in the Limitations Act, 2002 (Ontario), and such counsel expresses<BR>no opinion as to whether a court may find any provision of the Senior Debt Indenture to be unenforceable as an attempt to vary or exclude a limitation period under that Act.<BR>This opinion is given as of the date hereof and is limited to the laws of the Provinces of Ontario and the federal laws of Canada applicable thereto. In addition, this opinion is<BR>subject to customary assumptions about the trustee s authorization, execution and delivery of the Indenture and the genuineness of signatures and certain factual matters, all as<BR>stated in the letter of such counsel dated April 20, 2020, which has been filed as Exhibit 5.3 to Bank of Montreal s Form 6-K filed with the SEC and dated April 20, 2020.<BR>In the opinion of Mayer Brown LLP, when this pricing supplement has been attached to, and duly notated on, the master note that represents the notes, and the notes<BR>have been issued and sold as contemplated herein, the notes will be valid, binding and enforceable obligations of Bank of Montreal, entitled to the benefits of the Senior<BR>Indenture, subject to applicable bankruptcy, insolvency and similar laws affecting creditors  rights generally, concepts of reasonableness and equitable principles of general<BR>applicability (including, without limitation, concepts of good faith, fair dealing and the lack of bad faith). This opinion is given as of the date hereof and is limited to the laws of<BR>the State of New York. Insofar as this opinion involves matters governed by the laws of the Province of Ontario, or the laws of Canada applicable therein, Mayer Brown LLP has<BR>assumed, without independent inquiry or investigation, the validity of the matters opined on by Osler, Hoskin & Harcourt LLP, Canadian legal counsel for the issuer, in its<BR>opinion expressed above. This opinion is subject to customary assumptions about the trustee s authorization, execution and delivery of the Senior Indenture and the genuineness<BR>of signatures and to such counsel s reliance on the Bank of Montreal and other sources as to certain factual matters, all as stated in the legal opinion of Mayer Brown LLP dated<BR>April 20, 2020, which has been filed with the SEC as an exhibit to a report on Form 6-K by the Bank of Montreal on April 20, 2020.
SCAGY	I	2020/07/24	2020/07/24	3139869	6490955	US22550MGT09	DEP	5158				
SCAGY	I	2020/07/24	2020/07/24	3139870	6490955	US22550MGT09	UNW	3133				
SCAGY	I	2020/07/24	2020/07/24	3139888	6490955	US22550MGT09	GUR	3133	BPA			
SCAGY	I	2020/07/24	2020/07/24	3139889	6490955	US22550MGT09	BC	15527				In the opinion of Davis Polk & Wardwell LLP, as United States counsel to Credit Suisse, when the securities offered by this pricing supplement have<BR>been executed and issued by Credit Suisse and authenticated by the trustee pursuant to the indenture, and delivered against payment therefor,<BR>such securities will be valid and binding obligations of Credit Suisse, enforceable against Credit Suisse in accordance with their terms, subject to (i)<BR>applicable bankruptcy, insolvency and similar laws affecting creditors  rights generally, (ii) possible judicial or regulatory actions giving effect to<BR>governmental actions or foreign laws affecting creditors  rights and (iii) concepts of reasonableness and equitable principles of general applicability<BR>(including, without limitation, concepts of good faith, fair dealing and the lack of bad faith), provided that such counsel expresses no opinion as to the<BR>effect of fraudulent conveyance, fraudulent transfer or similar provision of applicable law on the conclusions expressed above. This opinion is given<BR>as of the date of this pricing supplement and is limited to the laws of the State of New York, except that such counsel expresses no opinion as to the<BR>application of state securities or Blue Sky laws to the securities. Insofar as this opinion involves matters governed by Swiss law, Davis Polk &<BR>Wardwell LLP has relied, without independent inquiry or investigation, on the opinion of Homburger AG, dated July 1, 2020 and filed by Credit<BR>Suisse as an exhibit to a Current Report on Form 6-K on July 1, 2020. The opinion of Davis Polk & Wardwell LLP is subject to the same<BR>assumptions, qualifications and limitations with respect to such matters as are contained in the opinion of Homburger AG. In addition, the opinion of<BR>Davis Polk & Wardwell LLP is subject to customary assumptions about the establishment of the terms of the securities, the trustee s authorization,<BR>execution and delivery of the indenture and its authentication of the securities, and the validity, binding nature and enforceability of the indenture<BR>with respect to the trustee, all as stated in the opinion of Davis Polk & Wardwell LLP dated July 1, 2020, which was filed by Credit Suisse as an<BR>exhibit to a Current Report on Form 6-K on July 1, 2020. Davis Polk & Wardwell LLP expresses no opinion as to waivers of objections to venue, the<BR>subject matter or personal jurisdiction of a United States federal court or the effectiveness of service of process other than in accordance with<BR>applicable law. In addition, such counsel notes that the enforceability in the United States of Section 10.08(c) of the indenture is subject to the<BR>limitations set forth in the United States Foreign Sovereign Immunities Act of 1976.
EDI_ENDOFFILE
